package com.qcris.linkify;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class SegundaActivity extends AppCompatActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_segunda);

		TextView parametroView = findViewById(R.id.parametro);
		parametroView.setText(getIntent().getStringExtra("USUARIO"));
	}
}
