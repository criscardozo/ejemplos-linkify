package com.qcris.linkify;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.util.Linkify;

import com.qcris.linkify.databinding.ActivityPrincipalBinding;

import java.util.regex.Pattern;

public class PrincipalActivity extends AppCompatActivity {

	private ActivityPrincipalBinding binding;

	@SuppressLint("SetTextI18n")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		binding = DataBindingUtil.setContentView(this, R.layout.activity_principal);

		binding.texto.setText("Esto sería un ejemplo de @alguien para ver un usuario. O también podría estar @este @otro.");
		binding.texto.setMovementMethod(LinkMovementMethod.getInstance());

		Pattern termsAndConditionsMatcher = Pattern.compile("@(\\w+)");
		Linkify.addLinks(binding.texto, termsAndConditionsMatcher, "usuario:");
	}

	@Override
	public void startActivity(Intent intent) {
		if (TextUtils.equals(intent.getAction(), Intent.ACTION_VIEW)) {
			if(intent.getData() != null) {
				String usuario = intent.getData().getSchemeSpecificPart().substring(1);
				Intent newIntent = new Intent(getApplicationContext(), SegundaActivity.class);
				newIntent.putExtra("USUARIO", usuario);
				startActivity(newIntent);
			}
		} else {
			super.startActivity(intent);
		}
	}
}
